import itertools
from pathlib import Path
from typing import Optional

import numpy as np
import pandas as pd
import seaborn as sns
from matplotlib import colors

HORMONES_DATA_PATH = Path("hormones_database.xlsx")
SHEET_NAME = "Database of clinically assayed "
COLUMN_FOR_INDEX = "Label"
N_AVOG = 6.022 * 10**23
COLUMNS_SUFFIXES = dict(zip(["female", "male"], ["_female", ""]))
ORGAN_ORDER = [
    "Hypothalamus",
    "Pineal",
    "Pituitary",
    "Thyroid",
    "Parathyroid",
    "Pancreas",
    "Adrenal Cortex",
    "Adrenal Cortex/Gonads",
    "Gonads",
    "Adrenal Medulla",
    "Adipose Tissue",
    "GI Tract",
    "Liver",
    "Kidney",
    "Heart",
    "Bone",
]
DUMMY_ROW_STRING = "000000"
COLUMNS_FOR_VOR_DICT = {
    "total_ug": "mass_male",
    "total_ug_female": "mass_female",
    "moles_circulating_hormones": "moles_male",
    "moles_circulating_hormones_female": "moles_female",
}
FACTORS_FOR_VOR_COLUMNS = [0.01, 0.01, 10**9, 10**9]
VORONOI_DATA_PATH = Path("Voronoi_data_and_tree.xlsx")


def load_hormones_data(
    hormones_data_path: Path = HORMONES_DATA_PATH,
    sheet_name: str = SHEET_NAME,
    index_column: str = COLUMN_FOR_INDEX,
) -> pd.DataFrame:
    df_hormones = pd.read_excel(
        hormones_data_path,
        sheet_name=sheet_name,
        skiprows=1,
        skipfooter=5,
        index_col=index_column,
    )
    df_hormones =df_hormones[df_hormones.for_ref_male.eq(1) | df_hormones.for_ref_female.eq(1)]
    df_hormones.columns = df_hormones.columns.str.replace(
        ".1", COLUMNS_SUFFIXES["female"]
    )
    
    add_moles_circulating_hormones(df_hormones)
    add_secreting_organ_and_color_column(df_hormones)
    return df_hormones


def add_moles_circulating_hormones(df_hormones: pd.DataFrame) -> None:
    for suff in COLUMNS_SUFFIXES.values():
        df_hormones[f"num_circulating_molecules{suff}"] = df_hormones[f"num_circulating_molecules{suff}"].replace("",0).astype(float)
        df_hormones[f"moles_circulating_hormones{suff}"] = (
            df_hormones[f"num_circulating_molecules{suff}"].astype(float) / N_AVOG
        )


def add_secreting_organ_and_color_column(df_hormones: pd.DataFrame) -> None:
    df_hormones["Secreting Organ"] = (
        df_hormones["Primarily secreted from: organ"]
        .str.replace(" Gland", "")
        .str.strip()
    )
    df_hormones["color"] = df_hormones["Secreting Organ"].map(get_color_dict(for_voronoi=True))
    # df_hormones[['Secreting Organ','color']].to_csv('sec_organ_colors.csv')


def get_color_dict(for_voronoi: bool = False) -> dict:
    cl1 = sns.color_palette("Set3", n_colors=7).as_hex()
    cl2 = [
        "#99d374",
        "#7fc97f",
        "#fddaec",
        "#7570b3",
        "#e6ab02",
        "#386cb0",
        "#f0027f",
        "#bf5b17",
        "#666666",
    ]
    
    color_dict = dict(zip(ORGAN_ORDER, cl1 + cl2))
    if for_voronoi:
        return {org: RBG_to_string(color_dict[org]) for org in color_dict.keys()}
    return color_dict

def RBG_to_string(c_lis):
    if type(c_lis) == str:
        # print(c_lis)
        c_lis = colors.to_rgb(c_lis)
    return "rgb{}".format(tuple((256 * np.array(c_lis)).round(0).astype(int)))


def add_to_color(c, n, fac=5):
    n = int(n)
    new_c = np.minimum(256,np.array(c[4:-1].split(', ')).astype(int) + n*fac)
    return 'rgb({}, {}, {})'.format(new_c[0], new_c[1], new_c[2])


def get_dataframe_for_voronoi_tree(df_hormones: pd.DataFrame) -> pd.DataFrame:
    df_vor_tree = preprocess_dataframe(df_hormones)
    df_vor_tree = add_repeats_and_colors(df_vor_tree)
    df_vor_tree = add_secreting_organ_rows(df_vor_tree)
    df_vor_tree = sort_dataframe(df_vor_tree)
    df_vor_tree = add_labels(df_vor_tree)
    return df_vor_tree


def preprocess_dataframe(df_hormones: pd.DataFrame) -> pd.DataFrame:
    df_vor_tree = df_hormones[["Secreting Organ", "color"]]
    df_vor_tree = df_vor_tree.dropna().sort_values("Secreting Organ")
    df_vor_tree = df_vor_tree.reset_index().rename(columns={'Label': "Name",})
    df_vor_tree = df_vor_tree.drop_duplicates()
    df_vor_tree = df_vor_tree[["Secreting Organ", "Name", "color"]].sort_values(["Secreting Organ", "Name"])
    return df_vor_tree


def add_repeats_and_colors(df_vor_tree: pd.DataFrame) -> pd.DataFrame:
    df_vor_tree["order"] = range(1, df_vor_tree.shape[0] + 1)
    df_vor_tree["repeats"] = list(
        itertools.chain.from_iterable(
            [list(range(k)) for k in df_vor_tree["Secreting Organ"].value_counts().sort_index()]
        )
    )
    df_vor_tree["color2"] = df_vor_tree.apply(lambda rw: add_to_color(rw.color, rw.repeats), axis=1)
    return df_vor_tree


def add_secreting_organ_rows(df_vor_tree: pd.DataFrame) -> pd.DataFrame:
    sec_org_color_dict = get_color_dict(for_voronoi=True)
    for sec_org in df_vor_tree["Secreting Organ"].unique():
        df_vor_tree = pd.concat(
            [
                df_vor_tree,
                pd.DataFrame(
                    {
                        "Secreting Organ": sec_org,
                        "Name": DUMMY_ROW_STRING,
                        "color": sec_org_color_dict[sec_org],
                        "color2": sec_org_color_dict[sec_org],
                    },
                    index=[0],
                ),
            ]
        ).reset_index(drop=True)
    return df_vor_tree


def sort_dataframe(df_vor_tree: pd.DataFrame) -> pd.DataFrame:
    df_vor_tree = df_vor_tree.sort_values(["Secreting Organ", "Name"], ascending=True)
    return df_vor_tree


def add_labels(df_vor_tree: pd.DataFrame) -> pd.DataFrame:
    df_vor_tree["label"] = df_vor_tree.apply(
        lambda row: np.nan
        if row.Name == DUMMY_ROW_STRING
        else "{}: c{:0.0f}".format(row.Name, row.order),
        axis=1,
    )
    df_vor_tree.loc[df_vor_tree.Name == DUMMY_ROW_STRING, "Name"] = np.nan
    df_vor_tree["Hormone"] = "Hormone"
    df_vor_tree = df_vor_tree.set_index("Hormone")
    df_vor_tree["c"] = ''
    df_vor_tree.loc[~df_vor_tree.order.isnull(), "c"] = "c" + df_vor_tree.loc[
        ~df_vor_tree.order.isnull(), "order"
    ].astype(int).astype(str)
    col_order = ["Secreting Organ", "label", "color2", "Name", "c"]
    df_vor_tree = df_vor_tree[col_order]
    return df_vor_tree


def get_voronoi_data(df_hormones: pd.DataFrame, df_vor_tree: Optional[pd.DataFrame] = None) -> pd.DataFrame:
    if df_vor_tree is None:
        df_vor_tree = get_dataframe_for_voronoi_tree(df_hormones)
    
    df_hormones["c"] = df_hormones.index.map(dict(zip(df_vor_tree.Name, df_vor_tree["c"])))
    df_hormones = df_hormones.copy().set_index("c")

    df_vor_values = pd.DataFrame(columns=COLUMNS_FOR_VOR_DICT.values(), index=df_hormones.index.drop_duplicates())
    for col, factor in zip(COLUMNS_FOR_VOR_DICT.keys(), FACTORS_FOR_VOR_COLUMNS):
        sex = COLUMNS_FOR_VOR_DICT[col].split("_")[1]
        df_vor_values[COLUMNS_FOR_VOR_DICT[col]] = df_hormones.loc[df_hormones[f"for_ref_{sex}"].eq(1), col] * factor
    
    df_vor_values["order"] = df_vor_values.index.str[1:].astype(int)
    df_vor_values = df_vor_values.sort_values("order")
    df_vor_values = df_vor_values.drop(columns="order")
    return df_vor_values


def extract_voronoi_tree_and_data(df_hormones: pd.DataFrame, path_to_save: Path = VORONOI_DATA_PATH) -> None:
    df_vor_tree = get_dataframe_for_voronoi_tree(df_hormones)
    df_vor_values = get_voronoi_data(df_hormones, df_vor_tree)

    with pd.ExcelWriter(path_to_save) as writer:
        df_vor_tree.to_excel(writer, sheet_name='Tree')
        for col in df_vor_values.columns:
            df_vor_values[col].to_excel(writer, sheet_name=col)