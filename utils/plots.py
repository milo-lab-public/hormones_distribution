import warnings
from typing import Literal

import matplotlib as mpl
import numpy as np
import pandas as pd
import seaborn as sns
from matplotlib import pyplot as plt
from matplotlib.axes import Axes
from matplotlib.lines import Line2D

from utils.preprocess import COLUMNS_SUFFIXES, ORGAN_ORDER, get_color_dict

warnings.filterwarnings("ignore")

def set_plotting_style() -> None:
    mpl.style.use("seaborn")
    sns.set_style("whitegrid")
    rc_pars = {
        "legend.facecolor": "white",
        "legend.framealpha": 0.85,
        "lines.linewidth": 3,
        "legend.edgecolor": "Black",
        "legend.frameon": True,
        "legend.fancybox": True,
        "legend.shadow": False,
        "figure.figsize": [10, 7],
        "figure.dpi": 300,
    }

    sns.set_context("talk", rc=rc_pars)

set_plotting_style()


def plot_moles_vs_mw_by_organ(
    df_hormones: pd.DataFrame,
    sex: Literal["male", "female"] = "female",
    y_par: str = "moles_circulating_hormones",
    organ_order: list = ORGAN_ORDER,
) -> None:
    plt.figure(figsize=(12, 12))
    ax = plt.gca()
    df = df_hormones[df_hormones[f"for_ref_{sex}"].eq(1)]
    y_par += COLUMNS_SUFFIXES[sex]
    df = df[df[y_par].notnull() & df[y_par].gt(0)]
    sns.scatterplot(
        data=df,
        x="Molecular weight (dalton)",
        y=y_par,
        hue="Secreting Organ",
        hue_order=organ_order,
        style="Hormone type",
        s=120,
        edgecolor="k",
        linewidth=1,
        zorder=10,
        ax=ax,
        palette=get_color_dict(),
    )

    ax.set(
        xlim=(0.3 * 10**2, 4 * 10**5),
        ylim=(3 * 10**-13, 10**-4),
        xscale="log",
        yscale="log",
        yticks=np.logspace(-12, -4, 9),
        xlabel="Molecular Weight [dalton]",
        ylabel="Amount of Circulating Hormone Molecules [mol]"
    )
    plot_log_lines(ax)
    customize_legend(ax, df)
    annotate_hormones(ax, df, y_par)
    plt.grid()
    plt.tight_layout()
    plt.savefig(f"Figures/moles_vs_MW_by_organ_{sex}.png", dpi=600)
    plt.savefig(f"Figures/moles_vs_MW_by_organ_{sex}.svg", dpi=600)



def plot_log_lines(ax: Axes) -> None:
    x_lims = np.array([0, 9])
    x = 10**x_lims
    x_lbl = 2 * 10**5
    for tot in np.logspace(0, -9, 10):
        lw = 1
        if (np.log10(tot) % 2 != 0) & (np.log10(tot) != -9):
            lw = 1.5
            y_lbl = tot / x_lbl * 0.8
            ax.text(
                x_lbl,
                y_lbl,
                r"$10^{{{}}}$ g".format(int(np.log10(tot))),
                rotation=325,
                ha="center",
                color="grey",
            )
        ax.plot(x, tot / x, ls="--", c="grey", alpha=0.5, lw=lw)


def customize_legend(ax: Axes, df_hormones: pd.DataFrame) -> None:
    hands, labs = ax.get_legend_handles_labels()
    n_type = len(df_hormones["Hormone type"].unique())
    h = hands[1 : -n_type - 1]
    l = labs[1 : -n_type - 1]
    emp_h = Line2D(
        [0],
        [0],
        marker="o",
        mec="k",
        mew=0.5,
        color="grey",
        alpha=0.5,
        markersize=0,
        ls="None",
    )
    h += [emp_h] * 3
    l += [""] * 3
    h += hands[-n_type:]
    l += labs[-n_type:]
    for i in range(len(h) - n_type, len(h)):
        h[i].set_facecolor("none")
    plt.legend(
        handles=h,
        labels=l,
        bbox_to_anchor=(1.02, 0.5),
        loc="center left",
        borderaxespad=0,
        markerscale=2,
    )
    ax.text(
        x=1.04,
        y=0.87,
        s=labs[0],
        fontsize=18,
        fontweight="bold",
        transform=ax.transAxes,
    )
    ax.text(
        x=1.04,
        y=0.285,
        s=labs[-n_type - 1],
        fontsize=18,
        fontweight="bold",
        transform=ax.transAxes,
    )


def annotate_hormones(ax: Axes, df_hormones: pd.DataFrame, y_par: str) -> None:
    fac_x_dict = {
        "default": 1,
        "lower_left": 0.95,
        "lower_right": 1.1,
        "right": 1.15,
        "bottom": 1,
        "upper_left": 0.98,
        "left": 0.87,
        "upper_right": 1.1,
    }
    fac_y_dict = {
        "default": 1.4,
        "lower_left": 0.85,
        "lower_right": 0.8,
        "right": 1,
        "bottom": 0.65,
        "upper_left": 1.3,
        "left": 1,
        "upper_right": 1.2,
    }
    ha_dict = {
        "default": "center",
        "lower_left": "right",
        "lower_right": "left",
        "right": "left",
        "bottom": "center",
        "upper_left": "right",
        "left": "right",
        "upper_right": "left",
    }
    loc_dict = dict(
        lower_left=[
            "Calcitonin",
            "Norepinephrin",
            "DHT",
            "Estriol",
        ],
        upper_left=[
            "Adiponectin",
            "Epinephrine",
            "DOC",
            "17-OH-Pregnenolone",
            "TSH",
            "GH",
            "Insulin",
        ],
        lower_right=["Calcitriol", "Glucagon"],
        right=[
            "FSH",
            "Corticosterone",
            "GLP-1 active",
            "PTH",
            "CCK",
            "17-OHP",
            "Inhibin B",
            "Pregnenolone",
            "BhCG",
            "Gastrin",
            "GLP-1 total",
            "Angiotensin 2",
            "EPO",
            "TRH",
            "Osteocalcin",
            "GHRH",
        ],
        upper_right=[
            "Progesterone",
            "Thyroxin",
            "Testosterone",
            "ACTH",
            "Aldosterone",
        ],
        left=[
            "Somatostatin",
            "VIP",
            "ADH",
            "11-Deoxycorticosterone",
            "Androstendione",
            "11-Deoxycortisol",
            "Estrone",
            "Dopamine",
            "FGF23",
            "DHEA",
            "Renin",
            "LHRH",
        ],
        bottom=[
            "Estradiol",
            "CRH",
        ],
    )
    for hr in df_hormones.index:
        if df_hormones.loc[hr, y_par] > 10**-13:
            fac_x, fac_y = fac_x_dict["default"], fac_y_dict["default"]
            ha = ha_dict["default"]
            for loc in loc_dict.keys():
                if hr in loc_dict[loc]:
                    fac_x, fac_y = fac_x_dict[loc], fac_y_dict[loc]
                    ha = ha_dict[loc]
            x_txt = df_hormones.loc[hr, "Molecular weight (dalton)"] * fac_x
            y_txt = df_hormones.loc[hr, y_par] * fac_y

            ax.text(
                x=x_txt,
                y=y_txt,
                s=hr,
                va="center",
                ha=ha,
                fontsize=14,
            )